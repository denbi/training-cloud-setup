# Training course cloud setup using Cloud9 and a proxy

This setup aims to make it easier for cloud training course participants to access their VM leaving more
of the valuable time for the actual content of the course rather than fiddling with SSH keys and clients for their operating system. A modern web browser is the only requirement.

## Prerequisites

The following things are needed to set up the training course infrastructure:

* **OpenStack project** for the training course with at least one public floating IP
* **Web domain** for your training course (e.g. `example.com`) with full DNS control

## General project setup

### Network setup

* Create network and subnet (optionally limit address allocation range)
* Create Router and use public network as gateway
* Add router interface for network

### Security groups

Modify the `default` security group to have the following rules:

| Direction | Ether Type | IP Protocol | Port Range | Remote IP Prefix | Remote Security Group | Actions     |
|-----------|------------|-------------|------------|------------------|-----------------------|-------------|
| Egress    | IPv4       | Any         | Any        | 0.0.0.0/0        | -                     | Delete Rule |
| Ingress   | IPv4       | TCP         | 1 - 65535  | -                | proxy                 | Delete Rule |
| Ingress   | IPv4       | TCP         | 22 (SSH)   | 0.0.0.0/0        | -                     |             |

Create security group `proxy` with the following rules:

| Direction | Ether Type | IP Protocol | Port Range    | Remote IP Prefix | Remote Security Group | Actions     |
|-----------|------------|-------------|---------------|------------------|-----------------------|-------------|
| Egress    | IPv4       | Any         | Any           | 0.0.0.0/0        | -                     | Delete Rule |
| Egress    | IPv6       | Any         | Any           | ::/0             | -                     | Delete Rule |
| Ingress   | IPv4       | TCP         | 1 - 65535     | -                | default               | Delete Rule |
| Ingress   | IPv4       | TCP         | 80 (HTTP)     | 0.0.0.0/0        | -                     | Delete Rule |
| Ingress   | IPv4       | TCP         | 443 (HTTPS)   | 0.0.0.0/0        | -                     | Delete Rule |
| Ingress   | IPv4       | TCP         | 30005 - 30254 | 0.0.0.0/0        | -                     |             |

## Proxy

Boot a VM as your proxy and assign it a public floating IP.

Log in and install the required software:
```
sudo apt-get install software-properties-common nginx apache2-utils
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get install python-certbot-nginx
```

### DNS

Add the following DNS records to your domain:

| Domain | Record type | Value |
| --- | --- | --- |
| example.com | A | floating IP of proxy |
| *.example.com | A | floating IP of proxy |

### Wildcard SSL certificate

Replace `DOMAIN` and `EMAIL` in the command below and then run it to request a certificate for your domain from letsencrypt.org.
```
sudo certbot -d DOMAIN -d '*.DOMAIN' --email EMAIL --agree-tos --manual --preferred-challenges dns certonly
```
Wildcard domain requests as this one require a _DNS validation_ procedure that will prompt
you to add several `TXT` records to your domain.

### Nginx proxy setup

Create a global single login for all participants of the training course to access the VMs (replace `USERNAME` with any name):

```
sudo htpasswd -c /etc/htpasswd USERNAME
```

Replace the contents of the file
`/etc/nginx/sites-available/default`
with:
```
server {
    listen 80;
    server_name _;
    return 301 https://$host$request_uri;
}
server {
    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;
    server_name ~^(?<subdomain>[0-9]+)\.example\.com$;
    location / {
      proxy_pass http://192.168.0.$subdomain:8181$request_uri;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      auth_basic "login";
      auth_basic_user_file "/etc/htpasswd";
    }
    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}

server {
    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;
    server_name example.com;
    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}
```

Replace all occurrences of `example.com` and `example\.com` with your DOMAIN and restart `nginx` using:
```
sudo systemctl restart nginx
```

### Landing page for participants

Copy [index.html](index.html) to `/var/www/html/` and replace all occurrences of `example.com` with your domain.

Later, participants will be able visit `example.com` and simply click on the VM number that has been assigned to them.

### SSH forwarding

Run [proxy-forward-ssh.sh](proxy-forward-ssh.sh) on the proxy as `root` to forward the SSH ports of all your VMs (thanks to Jan).

## Participant VM setup

The current setup for participant VMs is based on `Ubuntu 18.04`.
To reduce the amount of data that has to be downloaded by the setup routines when settings up a large number of machine simultaneously, it is recommended
to build the desired setup on a single virtual machine and create a virtual machine image from it.
Afterwards, any number identical copies can be booted without relying on any external downloads for the booting process to succeed.

### Cloud9 installation (including Docker)

To install Cloud9 as well as Docker to a VM you can use the script [vm-install-cloud9.sh](vm-install-cloud9.sh).
This script can either be supplied as _Customization Script_ (user data)
or by copying it to the VM and running it as _root_.

### Training course software setup

When installing any additional software or data to the VM, please
be aware that the Cloud9 interface is running under the `ubuntu` user.
The root directory of the Cloud9 file browser is `/home/ubuntu/`, so this
is what the user will be able to browse. Any files outside that directory
have to be edited/viewed using the command line.

### VM image creation

The following command helps to reduce the size of the resulting image by writing zeroes to the free disk space:
```
dd if=/dev/zero of=z bs=1M; sync; rm z;
```

Before creating the final VM image, please make sure to also clean up the following files:

* `/home/ubuntu/.bash_history` (stores command history)
* `/home/ubuntu/.viminfo` (stores vim history and buffers)
* `/root/.bash_history`
* `/root/.viminfo`

Depending on your setup there might be additional files to clean up.

## Booting VMs for participants

Boot VMs by simply booting the prepared image. Afterwards, identify the VM numbers by looking at the IP addresses the resulting VMs have, e.g. VM with IP `192.168.0.42` has the VM number `42`. A set of numbered pieces of paper to hand out might make it easier for you to directly assign VMs to users.
Users can then either visit `https://example.com` and click on the number to access their VM or they can type in `https://42.example.com` for direct access.
For administrators, SSH access is available via `ssh -p 30042 ubuntu@example.com -i ~/.ssh/my-private-ssh-key`.
