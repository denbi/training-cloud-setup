#!/bin/bash
# You can use this script as Customization Script (user data) for your VM
exec &> >(tee -a /var/log/setup.log)
ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get upgrade -y
DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential python nodejs python-pip apt-transport-https ca-certificates curl software-properties-common unzip tree
DEBIAN_FRONTEND=noninteractive apt-get remove tmux -y
DEBIAN_FRONTEND=noninteractive apt-get autoremove --purge -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
DEBIAN_FRONTEND=noninteractive apt-get install -y docker-ce
sudo usermod -a -G docker ubuntu
cd /tmp/
git clone https://github.com/c9/core /opt/c9sdk
mkdir /opt/c9sdk/.c9/
chown ubuntu:ubuntu /opt/c9sdk/.c9/
/opt/c9sdk/scripts/install-sdk.sh
su -c 'wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | bash' ubuntu
cat << 'EOF' > /usr/local/bin/cloud9
#!/bin/bash
cd /opt/c9sdk
NODE_ENV="production" node server.js --listen 0.0.0.0 -p 8181 -a ":" -w /home/ubuntu/ $@
EOF
chmod +x /usr/local/bin/cloud9
cat << 'EOF' > /etc/systemd/system/cloud9.service
[Unit]
Description=Cloud9 IDE
Requires=network.target
[Service]
Type=simple
User=ubuntu
ExecStart=/usr/local/bin/cloud9
Restart=on-failure
[Install]
WantedBy=multi-user.target
EOF
cat << 'EOF' > /home/ubuntu/.c9/user.settings
{
    "ace": {
        "@animatedScroll": true,
        "@antialiasedfonts": true,
        "@behavioursEnabled": true,
        "@cursorStyle": "ace",
        "@displayIndentGuides": true,
        "@fadeFoldWidgets": true,
        "@fontFamily": "Monaco, Menlo, Ubuntu Mono, Consolas, source-code-pro, monospace",
        "@fontSize": 14,
        "@highlightActiveLine": true,
        "@highlightGutterLine": true,
        "@highlightSelectedWord": true,
        "@keyboardmode": "default",
        "@mergeUndoDeltas": true,
        "@overwrite": false,
        "@printMarginColumn": 120,
        "@scrollPastEnd": 0.5,
        "@scrollSpeed": 2,
        "@selectionStyle": "line",
        "@showFoldWidgets": true,
        "@showGutter": true,
        "@showInvisibles": false,
        "@showLineNumbers": true,
        "@showPrintMargin": false,
        "@theme": "ace/theme/cloud9_night_low_color",
        "@useWrapMode": false,
        "@wrapBehavioursEnabled": false,
        "@wrapToView": true,
        "custom-types": {
            "json()": {
                "cwl": "yaml",
                "settings": "javascript"
            }
        },
        "statusbar": {
            "@show": true
        }
    },
    "breakpoints": {
        "@active": true
    },
    "build": {
        "@autobuild": false
    },
    "collab": {
        "@showbubbles": true
    },
    "debug": {
        "@autoshow": true,
        "@pause": 0
    },
    "findinfiles": {
        "@clear": true,
        "@consolelaunch": false,
        "@fullpath": false,
        "@project": "/",
        "@scrolldown": false
    },
    "general": {
        "@animateui": true,
        "@automerge": false,
        "@autosave": false,
        "@confirmmerge": true,
        "@downloadFilesAs": "auto",
        "@preview-navigate": false,
        "@preview-tree": false,
        "@revealfile": false,
        "@skin": "dark",
        "@treestyle": "default"
    },
    "key-bindings": {
        "@platform": "auto",
        "@preset": "default"
    },
    "language": {
        "@continuousCompletion": false,
        "@enterCompletion": true,
        "@hints": true,
        "@instanceHighlight": true,
        "@overrideMultiselectShortcuts": true
    },
    "metadata": {
        "@undolimit": 200
    },
    "openfiles": {
        "@hidetree": false,
        "@show": false
    },
    "output": {
        "@backgroundColor": "#003a58",
        "@foregroundColor": "#FFFFFF",
        "@keepOutput": false,
        "@nosavequestion": false,
        "@selectionColor": "#225477"
    },
    "preview": {
        "@default": "preview.browser",
        "@onSave": false,
        "@running_app": false
    },
    "projecttree": {
        "@hiddenFilePattern": "*.pyc, __pycache__, .*",
        "@scope": false,
        "@showhidden": false
    },
    "runconfig": {
        "@debug": false,
        "@saveallbeforerun": true,
        "@showruncfglist": false
    },
    "tabs": {
        "@asterisk": true,
        "@autoclosepanes": true,
        "@show": true,
        "@title": true
    },
    "terminal": {
        "@antialiasedfonts": true,
        "@backgroundColor": "#153649",
        "@blinking": false,
        "@defaultEnvEditor": false,
        "@fontfamily": "Ubuntu Mono, Menlo, Consolas, monospace",
        "@fontsize": 14,
        "@foregroundColor": "#FFFFFF",
        "@scrollback": 100000,
        "@selectionColor": "#515D77"
    },
    "tour": {
        "@default-complete": false
    },
    "welcome": {
        "@first": true
    }
}
EOF
chown ubuntu:ubuntu /home/ubuntu/.c9/user.settings
systemctl daemon-reload
systemctl enable cloud9
systemctl start cloud9

echo "###########################"
echo "###   SETUP FINISHED!   ###"
echo "###########################"
